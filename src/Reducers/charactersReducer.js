///
// This reducer is used for state related to characters 
///

import * as ActionTypes from '../Actions/ActionTypes';

const charactersBookmarked = JSON.parse(localStorage.getItem('___bookmarked_characters')) ? JSON.parse(localStorage.getItem('___bookmarked_characters')) : [];
const initialState = {
  searchValue: '',
  charactersFound: [],
  charactersBookmarked: charactersBookmarked
}

const charactersReducer = (state = initialState, action) => {

  if(action.type === ActionTypes.SEARCH_FOR_CHARACTERS){
    return{
      ...state,
      searchValue: action.payload.searchValue,
      charactersFound: action.payload.charactersFound
    }
  }

  if(action.type === ActionTypes.BOOK_CHARACTER){
    return {
      ...state,
      charactersBookmarked: action.payload
    }
  }

  if(action.type === ActionTypes.REMOVE_BOOKMARKED_CHARACTER){
    return{
      ...state,
      charactersBookmarked: action.payload,
    }
  }

  return state;
}

export default charactersReducer;
