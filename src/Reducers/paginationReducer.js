///
// This reducer is used for state related to pagination
///

import * as ActionTypes from '../Actions/ActionTypes';

const charactersBookmarked = JSON.parse(localStorage.getItem('___bookmarked_characters')) ? JSON.parse(localStorage.getItem('___bookmarked_characters')) : [];
const limit = 13;

const initialState = {
  pageNumber: 1,
  prevPage: false,
  nextPage: charactersBookmarked.length >= limit,
  offset: 0,
  limit: limit
}

const paginationReducer = (state = initialState, action) => {
  if(action.type === ActionTypes.UPDATE_PAGINATE_STATE){
    return{
      ...state,
      pageNumber: action.payload.pageNumber,
      prevPage: action.payload.prevPage,
      nextPage: action.payload.nextPage,
      offset: action.payload.offset
    }
  }

  return state;
}

export default paginationReducer;
