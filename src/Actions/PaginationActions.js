import axios from 'axios';
import * as ActionTypes from './ActionTypes';


///
// Paginate true characters that are bookmarked
///
export const paginateCharactersBooked = (data) => {
  return (dispatch) => {
    const [pageNumber, offset] = (data.direction == 'next')?
          [data.pageNumber + 1, data.offset + (data.limit-1)]:
          [data.pageNumber - 1, data.offset - (data.limit-1)];

    dispatch({
      type: ActionTypes.UPDATE_PAGINATE_STATE,
      payload: {
        nextPage:data.charactersBookmarked.length-offset >= data.limit,
        prevPage: offset > 0,
        pageNumber: pageNumber,
        offset:offset
      }
    })
  }
}


///
// Paginate true characters that are found on search
///
export const paginateCharactersFound = (data) => {
  return (dispatch) => {

    let button = data.element.target;
    button.disabled = true;

    const [pageNumber, offset] = (data.direction == 'next')?
    [data.pageNumber + 1, data.offset + (data.limit-1)]:
    [data.pageNumber - 1, data.offset - (data.limit-1)];

    axios.get("https://gateway.marvel.com/v1/public/characters?nameStartsWith=" + data.searchValue + "&limit=" + data.limit + "&offset="+ offset +"&ts=1&apikey=0b80a07b4dbdc73eaf874a93e911f33c&hash=b17b64820076cdfd01380a565f37ce54")
    .then( response => {
      dispatch({
        type: ActionTypes.UPDATE_PAGINATE_STATE,
        payload: {
          nextPage: response.data.data.results.length == data.limit,
          prevPage: offset > 0,
          pageNumber: pageNumber,
          offset: offset
        }
      });

      dispatch({
        type: ActionTypes.SEARCH_FOR_CHARACTERS,
        payload: {
          searchValue: data.searchValue,
          charactersFound: response.data.data.results
        }
      })

      button.disabled = false;
    })
    .catch(error => {throw error;});
  }
}
