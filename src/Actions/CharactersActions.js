import axios from 'axios';
import * as ActionTypes from './ActionTypes';

///
// Search for marvel characters
///
export const searchForCharacters = (data) => {
  let ajaxDelay = null;

  return (dispatch, getState) => {
    clearTimeout(ajaxDelay);

    // Delay ajax call for better user experiance
    ajaxDelay = setTimeout(() =>{

      // Send ajax call if search field have some value
      if(data.searchValue !== ''){
        axios.get("https://gateway.marvel.com/v1/public/characters?nameStartsWith=" + data.searchValue + "&limit=" + data.limit + "&offset="+ data.offset +"&ts=1&apikey=0b80a07b4dbdc73eaf874a93e911f33c&hash=b17b64820076cdfd01380a565f37ce54")
        .then( response => {
          dispatch({
            type: ActionTypes.SEARCH_FOR_CHARACTERS,
            payload: {
              charactersFound: response.data.data.results,
              searchValue: data.searchValue,
            }
          });
          dispatch({
            type: ActionTypes.UPDATE_PAGINATE_STATE,
            payload:{
              nextPage: response.data.data.results.length == data.limit,
              prevPage: false,
              pageNumber: 1,
              offset: 0
            }
          });
        })
        .catch(error => {throw error;});

      // If search field is empty update state
      }else{
        dispatch({
          type: ActionTypes.SEARCH_FOR_CHARACTERS,
          payload: {
            searchValue: data.searchValue,
            charactersFound: []
          }
        });
        dispatch({
          type: ActionTypes.UPDATE_PAGINATE_STATE,
          payload:{
            prevPage: false,
            nextPage: data.charactersBookmarked.length >= data.limit,
            pageNumber: 1,
            offset: 0
          }
        });
      }

    },500);

  }
}


///
// Bookmark marvel character
///
export const bookCharacter = (data) => {
  return (dispatch) => {

    // Update button
    data.element.target.classList.remove("btn-red");
    data.element.target.classList.add("btn-green");
    data.element.target.innerHTML = 'Booked';

    // Get current character
    const character = data.charactersFound.find((element)=>element.id == data.id);

    // If we dont have bookmarks update bookmark localstorage with new character.
    if(data.charactersBookmarked.length <= 0){
      localStorage.setItem('___bookmarked_characters', JSON.stringify([character]));
      dispatch({type: ActionTypes.BOOK_CHARACTER, payload: [character]});

    // If we alredy have bookmarks
    }else{
      const newArray = [...data.charactersBookmarked, character];
      localStorage.setItem('___bookmarked_characters', JSON.stringify(newArray));
      dispatch({type: ActionTypes.BOOK_CHARACTER, payload: newArray});
    }

  }
}


///
// Remove bookmarked marvel character
///
export const removeCharacter = (data) => {
  return (dispatch) => {
    let charactersBookmarked = [...data.charactersBookmarked];
    charactersBookmarked = charactersBookmarked.filter((el)=> el.id != data.id);

    if(charactersBookmarked.length === 0){
       localStorage.removeItem('___bookmarked_characters');
       dispatch({type: ActionTypes.REMOVE_BOOKMARKED_CHARACTER, payload: charactersBookmarked});
       return;
    }

    localStorage.setItem('___bookmarked_characters', JSON.stringify(charactersBookmarked));
    let pageNumber = data.pageNumber;
    let offset = data.offset;
    let nextPage = data.nextPage;
    let prevPage = data.prevPage;

    // Make sure to update pagination data when removing booked character
    if(charactersBookmarked.length <= offset || charactersBookmarked.length<=data.limit-1){
       pageNumber = pageNumber-1;
       offset = offset-(data.limit-1);
       nextPage = false;
       prevPage = (pageNumber==1)? false: true;
       [pageNumber, offset, nextPage, prevPage] = (pageNumber == 0)? [1,0,false,false] : [pageNumber, offset, nextPage, prevPage];
    }

    // Dispatch actions to update bookmarked characters and pagination data
    dispatch({type: ActionTypes.REMOVE_BOOKMARKED_CHARACTER, payload: charactersBookmarked})
    dispatch({
      type: ActionTypes.UPDATE_PAGINATE_STATE,
      payload: {
        pageNumber: pageNumber,
        offset: offset,
        nextPage: nextPage,
        prevPage: prevPage
      }
    });
  }
}
