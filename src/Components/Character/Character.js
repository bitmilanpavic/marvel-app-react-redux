import React, { Component } from 'react';

const Character = (props) => {

  // For button determine on click action, text and classes based if we are showing bookmarked on founded characters
  let [buttonAction, buttonText, classes] = (props.searchValue !== '')?
      [props.bookCharacter, 'Bookmark', 'btn btn-red'] : [props.removeCharacter, 'Remove', 'btn btn-green pointer'];

  [buttonAction, buttonText, classes] = (props.booked >= 0 && props.searchValue !== '')?
  [()=>{}, 'Booked', 'btn btn-green'] : [buttonAction, buttonText, classes];


  return(
    <div className="app-character">
      <div className="app-character__image" style={{backgroundImage:"url(" + props.thumbnailPath + "/standard_fantastic." + props.thumbnailExtension + ")"}}></div>
      <h4 className="app-character__name">{props.name}</h4>
      <button className={classes} onClick={(el)=>buttonAction({
        element: el,
        id: props.id,
        charactersFound: props.charactersFound,
        charactersBookmarked: props.charactersBookmarked,
        limit: props.limit,
        offset: props.offset,
        pageNumber: props.pageNumber,
        prevPage: props.prevPage,
        nextPage: props.nextPage
      })}>{buttonText}</button>
    </div>
  )

}


export default Character;
