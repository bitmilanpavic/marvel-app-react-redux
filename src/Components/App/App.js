import React from 'react';
import './App.css';

import Characters from '../../Containers/Characters/Characters';
import Pagination from '../../Containers/Pagination/Pagination';

const App = () => {
  return(
    <div className="app-container">
      <h1 className="text-center">Marvel App</h1>
      <Characters />
      <Pagination />
    </div>
  )
}

export default App;
