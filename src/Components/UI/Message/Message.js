import React from 'react';

const Message = (props) => <p className="text-center">{props.children}</p> 

export default Message;
