import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../Actions/PaginationActions';

class Pagination extends Component{
  render(){
    const classPrev = (!this.props.prevPage)? 'app-pagination__prev btn disabled' : 'app-pagination__prev btn btn-blue';
    const classNext = (!this.props.nextPage)? 'app-pagination__next btn disabled' : 'app-pagination__next btn btn-blue';
    const buttonAction = (this.props.searchValue !== '')? this.props.paginateCharactersFound: this.props.paginateCharactersBooked;

    // Dont show pagination if there's nothing to paginate
    if(!this.props.prevPage && !this.props.nextPage){
      return '';
    }

    return(
      <div className="app-pagination">
        <button className={classPrev} onClick={(el)=>buttonAction({
          element: el,
          direction: "prev",
          pageNumber: this.props.pageNumber,
          limit: this.props.limit,
          offset: this.props.offset,
          charactersBookmarked: this.props.charactersBookmarked,
          searchValue: this.props.searchValue
        })} disabled={!this.props.prevPage}>Prev</button>

        <span className="app-pagination__page">Page ({this.props.pageNumber})</span>

        <button className={classNext} onClick={(el)=>buttonAction({
          element: el,
          direction: "next",
          pageNumber: this.props.pageNumber,
          limit: this.props.limit,
          offset: this.props.offset,
          charactersBookmarked: this.props.charactersBookmarked,
          searchValue: this.props.searchValue
        })} disabled={!this.props.nextPage}>Next</button>
      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    searchValue: state.charactersReducer.searchValue,
    charactersFound: state.charactersReducer.charactersFound,
    charactersBookmarked: state.charactersReducer.charactersBookmarked,
    pageNumber: state.paginationReducer.pageNumber,
    prevPage: state.paginationReducer.prevPage,
    nextPage: state.paginationReducer.nextPage,
    offset: state.paginationReducer.offset,
    limit: state.paginationReducer.limit
  }
}

const mapDispatchToProps = (dispatch) =>{
  return{
    paginateCharactersBooked: (data) => dispatch(Actions.paginateCharactersBooked(data)),
    paginateCharactersFound:  (data) => dispatch(Actions.paginateCharactersFound(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pagination);
