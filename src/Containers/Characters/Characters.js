import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../Actions/CharactersActions';
import Message from '../../Components/UI/Message/Message';
import Character from '../../Components/Character/Character';

class Characters extends Component{

  render(){
    // Get our characters,if search fields is empty get bookmarked characters, if not get founded characters
    const errorMessage = ( this.props.searchValue !== '' )? 'No characters found.' :   'No characters bookmarked.';
    let characters = ( this.props.searchValue !== '' )? this.props.charactersFound: this.props.charactersBookmarked.filter((el, index)=>{
      if( (index+1) > this.props.offset && (index+1) <= this.props.offset+this.props.limit){
        return el;
      }});

    // Remove last element if number of characters is equal to limit
    // Last element is used only to determine if there is 'next page'
    characters = (characters.length == this.props.limit)? characters.slice(0, -1): characters;

    return(
      <React.Fragment>

        <input type="text" className="app-search"
          onKeyUp={(element) => this.props.searchForCharacters({
            searchValue: element.target.value,
            limit: this.props.limit,
            offset: this.props.offset,
            charactersBookmarked: this.props.charactersBookmarked
          })} />

          <div className="app-characters">
            {
              ( characters.length > 0 )?
                characters.map((character, index) => {
                  let booked = this.props.charactersBookmarked.findIndex((el) => el.id == character.id);
                  return <Character
                    id={character.id}
                    name={character.name}
                    thumbnailPath={character.thumbnail.path}
                    thumbnailExtension={character.thumbnail.extension}
                    charactersFound={this.props.charactersFound}
                    charactersBookmarked={this.props.charactersBookmarked}
                    bookCharacter={this.props.bookCharacter}
                    removeCharacter={this.props.removeCharacter}
                    limit={this.props.limit}
                    offset={this.props.offset}
                    pageNumber={this.props.pageNumber}
                    prevPage={this.props.prevPage}
                    nextPage={this.props.nextPage}
                    searchValue={this.props.searchValue}
                    booked={booked}
                    key={index} />
              }):
              <Message>{errorMessage}</Message>
          }
        </div>

      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) =>{
  return {
    searchValue: state.charactersReducer.searchValue,
    charactersFound: state.charactersReducer.charactersFound,
    charactersBookmarked: state.charactersReducer.charactersBookmarked,
    pageNumber: state.paginationReducer.pageNumber,
    prevPage: state.paginationReducer.prevPage,
    nextPage: state.paginationReducer.nextPage,
    offset: state.paginationReducer.offset,
    limit: state.paginationReducer.limit
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    searchForCharacters: (data) => dispatch(Actions.searchForCharacters(data)),
    bookCharacter: (data) => dispatch(Actions.bookCharacter(data)),
    removeCharacter: (data) => dispatch(Actions.removeCharacter(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Characters);
